require 'application_view'

class HomeView < ApplicationView
  def index
    header
    body do
      line('Welcome to NUS TECHNOLOGY RoR Training')
      line
      line('Trainees:')
      # TODO: Place your code here
       @locals[:a].each do |value|
        line("#{value[:id]} " " #{value[:name]}- #{value[:email]}")
        
      end


    end
    footer
  end

  def about
    header
    body do
      line('Web Browser Simulator Version 0.1')
      line('Contact Support: hansfordnguyen@hotmail.com')
    end
    footer
  end

  def page_not_found
    header
    body do
      line('404')
      line('PAGE NOT FOUND!')
    end
    footer
  end
end
