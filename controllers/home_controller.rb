require 'application_controller'

class HomeController < ApplicationController
  def index
     # TODO: Place your code here
    @trainees = Trainee.all
    render :index, {a: @trainees}
  end

  def about
    render :about
  end

  def page_not_found
    render :page_not_found
  end
end
